$(document).ready(function () {
  $("#boardAkhir").hide();
  $("#tryAgain").hide();
  //gravatar
  var human;
  var ai = '/static/img/grav6.PNG';
  
  var humanCode, aiCode;  // kode player

  // board
  var position = [[],
  "x","x","x",
  ".",".",".",
  "o","o","o"];

  var moves = [[0]];      // bisa move gak
  var avail = [[],[2,4,5],[1,3,5],[2,6,5],[1,7,5],[1,2,3,4,6,7,8,9],[3,9,5],[4,8,5],[7,9,5],[6,8,5]]; // available move
  var difficulty, avatar; // option
  var asalPawn, tujuanPawn;
  var pawnNow ;
  var playerTurn;
  var randNum;
  var winner = false;
  var hasMuv = false;
  var gameOverValue = false;

  var tantangan = [1,2];
  var counter = 4;
  localStorage.removeItem("startBy");
  localStorage.removeItem("difficulty");
  localStorage.removeItem("avatar");
  $("#alert").hide();

  $("#siapMain").click(function() {
    /*
    Cek sampai ketiga pilihan terisi
    */
    if (localStorage.getItem("startBy") == null) {
      $("#alert").text("Pilih pemain pertama dulu!");
      $("#alert").show();
    } else if (localStorage.getItem("difficulty") == null) {
      $("#alert").text("Pilih tingkat kesulitannya dulu!");
      $("#alert").show();
    } else if (localStorage.getItem("avatar") == null) {
      $("#alert").text("Pilih avatar kamu dulu!");
      $("#alert").show();
    } else {
      $("#boardAkhir").show();
      $("#chooseAkhir").hide();
      $("#judulUtama").text("Game Started!");
      initAvailMove();
      setInit();
      setBoard();
      gameStart();
    }
  });

  // Opsi pertama
  $("#cAI").click(function() {
    startBy = $(this).text();
    localStorage.setItem('startBy', startBy);
    console.log('Start By : ' + startBy);
  });

  $("#cUS").click(function() {
    startBy = $(this).text();
    localStorage.setItem('startBy', startBy);
    console.log('Start By : ' + startBy);
  });

  // Opsi kedua
  $("#cEas").click(function() {
    difficulty = $(this).text();
    localStorage.setItem('difficulty', difficulty);
    console.log('Difficulty : ' + difficulty);
  });

  $("#cMed").click(function() {
    difficulty = $(this).text();
    localStorage.setItem('difficulty', difficulty);
    console.log('Difficulty : ' + difficulty);
  });

  $("#cHrd").click(function() {
    difficulty = $(this).text();
    localStorage.setItem('difficulty', difficulty);
    console.log('Difficulty : ' + difficulty);
  });

  // Opsi ketiga
  $("#cg1").click(function() {
    avatar = $('#cg1 img').attr('src');
    localStorage.setItem('avatar', avatar);
    console.log('avatar : ' + avatar);
  });

  $("#cg2").click(function() {
    avatar = $('#cg2 img').attr('src');
    localStorage.setItem('avatar', avatar);
    console.log('avatar : ' + avatar);
  });

  $("#cg3").click(function() {
    avatar = $('#cg3 img').attr('src');
    localStorage.setItem('avatar', avatar);
    console.log('avatar : ' + avatar);
  });

  $("#cg4").click(function() {
    avatar = $('#cg4 img').attr('src');
    localStorage.setItem('avatar', avatar);
    console.log('avatar : ' + avatar);
  });

  $("#cg5").click(function() {
    avatar = $('#cg5 img').attr('src');
    localStorage.setItem('avatar', avatar);
    console.log('avatar : ' + avatar);
  });

  // Isi moves
  function initAvailMove() {
    for (i = 1; i < 10; i++) {
      moves.push([0]);
      for (j = 1; j < 10; j++) {
        if (avail[i].includes(j)){
          moves[i].push.apply(moves[i],[true]);
        } else {
          moves[i].push.apply(moves[i],[false]);
        }
      }
    }
  }

  /* set nilai playerTurn
    isi array player sama enemy
    manggil setBoard()
  */
  function setInit() {
    startBy = localStorage.getItem("startBy");
    difficulty = localStorage.getItem("difficulty");
    human = localStorage.getItem("avatar");
    if (startBy == "AI") {
      playerTurn = false;
      humanCode = "x";
      aiCode = "o";
      console.log("AI mulai duluan");
      setBoard();
    } else {
      playerTurn = true;
      humanCode = "o";
      aiCode = "x";
      console.log("human mulai duluan");
      setBoard();
    }
  }

  /*
    set papan board sesuai pionnya
  */
  function setBoard() {
    if (startBy == "USER") {
      $("#g1").attr("src",ai);
      $("#g2").attr("src",ai);
      $("#g3").attr("src",ai);
      $("#g7").attr("src",human);
      $("#g8").attr("src",human);
      $("#g9").attr("src",human);
    } else {
      $("#g1").attr("src",human);
      $("#g2").attr("src",human);
      $("#g3").attr("src",human);
      $("#g7").attr("src",ai);
      $("#g8").attr("src",ai);
      $("#g9").attr("src",ai);
    }
  }

  function gameStart() {
    normalizeButton();
    asalPawn = null;
    if ((!playerTurn) && (winner == false)) {
      if (difficulty == "Easy") {
        aiMove();
      } else {
        aiMoveMinimax();
      }
      playerTurn = !playerTurn;
      winner = wins(position, aiCode);
    }

    if (winner) {
      gameOver(winner);
    } else {
      $("#turns").text("YOUR TURN!");
      normalizeButton();
    }
  }

  function normalizeButton() {
    for (var i = 1; i < 10; i++) {
      if (position[i] != humanCode) {
        $("#b" + i).toggleClass('w3-white w3-grey', false);
      }
    }
  }

  // Kalau tombol diklik
  $("#boardAkhir button").click(function() {
    var sekarang = $(this).attr("id")[1];
    if (gameOverValue) {

    } else if (position[sekarang] == humanCode) {
      normalizeButton()
      $("#petunjuk").text("");
      asalPawn = sekarang;
      avail[asalPawn].forEach(x => {
        if (canMove(position, asalPawn, x)) {
          $("#b" + x).toggleClass('w3-white w3-grey', true);
        }
      });
    } else if ((position[sekarang] == ".") && (asalPawn != null)) {
      if (!canMove(position, asalPawn, sekarang)) {
        $("#petunjuk").text("You can't move your pawn from position " + asalPawn + " to " + sekarang);
      } else {
        tujuanPawn = sekarang;
        move(position, asalPawn, tujuanPawn);
        winner = wins(position, humanCode);
        if (winner) {
          gameOver(winner);
        } else {
          playerTurn = !playerTurn;
          gameStart();
        }
      }
    } else {
      $("#petunjuk").text("This pawn is not yours");
    }
  });

  function gameOver(winner) {
    $("#turns").text("GAME OVER!");
    let penutup;
    let petunjuk;
    console.log(winner);
    if (winner == aiCode) {
      penutup = "Sorry, you lost.";
      petunjuk = ":(";
    } else if (winner == humanCode) {
      penutup = "Congratulation, You win!";
      petunjuk = ":)";
    }
    $("#judulUtama").text(penutup);
    $("#tryAgain").show();
    $("#petunjuk").text(penutup);
    $("#petunjuk2").text(petunjuk);
    gameOverValue = true;
  }

  $("#tryAgain").click(function() {
    location.reload();
  });

  function wins(state, player) {
    win_state = [
      [state[1],state[2],state[3]],
      [state[4],state[5],state[6]],
      [state[7],state[8],state[9]],
      [state[1],state[4],state[7]],
      [state[2],state[5],state[8]],
      [state[3],state[6],state[9]],
      [state[1],state[5],state[9]],
      [state[3],state[5],state[7]]];

    for (var h = 0; h < 8; h++) {
      // if ((win_state[h][0] == player) && (win_state[h][1] == player) && (win_state[h][2] == player)) {
      if (JSON.stringify([player,player,player]) == JSON.stringify(win_state[h])) {
        if ((player == "x") && (h == 0)) {
          return false;
        } else if ((player == "o") && (h == 2)) {
          return false;
        }
        return player;
      }
    }
    return false;
  }

  function canMove(state, a, b) {
    return ((moves[a][b]) && (state[b] == "."));
  }

  function removeObstacle() {
    moves[tantangan[0]][tantangan[1]] = 1;
    moves[tantangan[1]][tantangan[0]] = 1;
    console.log("can move from " + tantangan[0] + " to " + tantangan[1]);
    $("#" + tantangan[0] + tantangan[1]).show();
    $("#" + tantangan[1] + tantangan[0]).show();
  }

  function addObstacle() {
    var a = getRandom(1,9);
    var b = avail[a][getRandom(0,avail[a].length - 1)];

    while (moves[a][b] == false) {
      a = getRandom(1,9);
      b = avail[a][getRandom(0,avail[a].length - 1)];
    }
    moves[a][b] = 0;
    moves[b][a] = 0;
    tantangan[0] = a;
    tantangan[1] = b;

    console.log("cannot move from position " + tantangan[0] + " to " + tantangan[1]);
    $("#petunjuk2").text("The path from position " + a + " to " + b + " is removed!");
    $("#" + tantangan[0] + tantangan[1]).hide();
    $("#" + tantangan[1] + tantangan[0]).hide();
  }

  function move(state, asal, tujuan) {
    var temp = state[asal];
    state[asal] = state[tujuan];
    state[tujuan] = temp;

    var tempImg = $("#g" + asal).attr("src");
    $("#g" + asal).attr("src", $("#g" + tujuan).attr("src"));
    $("#g" + tujuan).attr("src", tempImg);
    
    if (playerTurn) {
      $("#petunjuk").text("You moved the pawn from position " + asal + " to " + tujuan);
    } else {
      $("#petunjuk").text("AI moved the pawn from position " + asal + " to " + tujuan);
    }
    // set counter
    counter = counter - 1;
    if (difficulty == "Hard") {
      if (counter == 0) {
        removeObstacle();
        counter = 2;
        addObstacle();
      }
    }
  }

  function heuristic(state, player) {
    var state_now = [
    [state[1],state[2],state[3]],
    [state[4],state[5],state[6]],
    [state[7],state[8],state[9]],
    [state[1],state[4],state[7]],
    [state[2],state[5],state[8]],
    [state[3],state[6],state[9]],
    [state[1],state[5],state[9]],
    [state[3],state[5],state[7]]];
    
    var maks = -5;
    var tempp;
    for (var i = 0; i < 8; i++) {
      if ((i == 0) && (player == "x")) {
        tempp = 0;
      } else if ((i == 2) && (player == "o")) {
        tempp = 0;
      } else {
        temp = state_now[i].filter(x => x == player);
      }
      
      if (player == humanCode) {
        tempp = tempp * -1;
      }
      if (maks < tempp) {
        maks = tempp;
      }
    }
    return maks;
  }

  function minimax(state, depth, player) {
    // TODO
    let best;
    if (player) {
      best = [-1, -1, 0];
    } else {
      best = [-1, -1, 0];
    }

    if (wins(state, humanCode)) {
      return [-1, -1, -3];
    } else if (wins(state, aiCode)) {
      return [-1, -1, 3];
    }

    if (depth == 0) {
      if (player){
        return [-1, -1, heuristic(state, humanCode)];
      } else {
        return [-1, -1, heuristic(state, aiCode)];
      }
    }

    for (var x = 1; x < 10; x++) {
      if (((player) && (state[x] != humanCode)) || ((!player) && (state[x] != aiCode)))
        continue;
      var tempState = state.slice();
      for (let y of avail[x]) {
        if (!canMove(tempState, x, y)) {
          continue;
        }

        tempMove(tempState, x, y);
        let score = minimax(tempState, depth - 1, !player);
        tempMove(tempState, x, y);
        score[0] = x;
        score[1] = y;

        if (best[0] == -1) {
          best = score.slice();
        } else if (player) {
          if (score[2] < best[2]) {
            best = score.slice();
          }
        } else {
          if (score[2] > best[2]) {
            best = score.slice();
          }
        }
      }
    }
    return best;
  }

  function aiMoveMinimax() {
    console.log("giliran AI");
    var hasil = minimax(position, 5, false);
    var a = hasil[0];
    var b = hasil[1];
    move(position, a, b);
  }

  function tempMove(state, asal, tujuan) {
    let tempoPost = state[asal];
    state[asal] = state[tujuan];
    state[tujuan] = tempoPost;
  }

  function aiMove() {
    // TODO
    asalPawn = getRandom(1,9);
    while (hasMove(asalPawn) == false || position[asalPawn] != aiCode) {
      asalPawn = getRandom(1,9);
    }

    tujuanPawn = avail[asalPawn][getRandom(0,avail[asalPawn].length - 1)]
    while (canMove(position,asalPawn,tujuanPawn) == false || (isPawn(tujuanPawn)) ) {
      tujuanPawn = avail[asalPawn][getRandom(0,avail[asalPawn].length - 1)]
    }
    move(position, asalPawn, tujuanPawn);
  }

  function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
  }

  function isPawn(idx) {
    if (position[idx] != ".") {
      return true
    }
    return false
  }

  function hasMove(asal){
    for (let iter = 0; iter < avail[asal].length; iter++) {
      let dest = avail[asal][iter];
      if ((canMove(position, asal, dest)) && (!isPawn(dest))) {
        return true;
      }
    }
    return false;
  }
});      //////////////// ENDING JQUERY
