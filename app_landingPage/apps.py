from django.apps import AppConfig


class AppLandingpageConfig(AppConfig):
    name = 'app_landingPage'
