from django.urls import path
from app_landingPage import views

app_name = 'app_landingPage'
urlpatterns = [
	path('', views.landingPage, name='landingPage'),
	path('home', views.landingPage, name='landingPage'),
	# path('signup/', views.signUp, name='sign_up'),
	# path('signin/', views.signIn, name='sign_in'),
]
