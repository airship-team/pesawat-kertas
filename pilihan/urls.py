from django.urls import path
from pilihan import views

app_name = 'pilihan'
urlpatterns = [
	path('', views.pilihan, name='choosen_page'),
]