import random
#fun fact : 
	# Buatan peha. katanya dulu pas ddp 2 mikirin solusi ini bisa 10 jam. Pfft bego

	#     5
	#    454
	#   34543
	#  2345432
	# 123454321

class Board:
	def __init__(self):
		self.position = []
		self.move = [[False]]
		self.tantangan = [1,2]
		self.avail = [[],[2,4,5],[1,3,5],[2,6,5],[1,7,5],[1,2,3,4,6,7,8,9],[3,9,5],[4,8,5],[7,9,5],[6,8,5]]

		# Bikin array yang muat 10 x 10
		for i in range(1,10):
			self.move.append([False])				# Untuk index 0 pasti False
			for j in range(1,10):
				if j in self.avail[i]:
					self.move[i].extend([1])
				else:
					self.move[i].extend([0])

	def hasMove(self,pawn):
		pos = pawn.getPos()
		for dest in self.avail[pos]:
			if self.getAvailableMove(pos, dest) and (not self.position[dest].isPawn()):
				return True
		return False

	def getAvailableMove(self,a,b):
		# if self.move[a][b]:
		# 	print("CAN MOVE -->  {} to {}".format(a,b))
		return self.move[a][b]

	# Hapus tantangan dari yang udah disimpan di variabel tantangan
	def removeObstacle(self):
		self.move[self.tantangan[0]][self.tantangan[1]] = True
		self.move[self.tantangan[1]][self.tantangan[0]] = True

	# Fungsi untuk nambah obstacle
	# a --> asal pergerakan
	# b --> tujuan pergerakan
	def addObstacle(self):
		a = random.randint(1,10)
		b = random.randint(1,10)
		while (self.move[a][b] == 0):
			a = random.randint(1,10)
			b = random.randint(1,10)
		self.move[a][b] = False
		self.move[b][a] = False
		self.tantangan[0] = a
		self.tantangan[1] = b
		
		self.move[self.tantangan[0]][self.tantangan[1]] = False

	# update posisi pion-pionan
	def update(self, position):
		self.position = position

	# Check apakah sudah ada yang menang
	def check(self):
		if self.position[1].getKode() == self.position[2].getKode() == self.position[3].getKode() != 'x' != ".":
			return [True,self.position[1]]
		elif self.position[4].getKode() == self.position[5].getKode() == self.position[6].getKode() != ".":
			return [True,self.position[4]]
		elif self.position[7].getKode() == self.position[8].getKode() == self.position[9].getKode() != 'o' != ".":
			return [True,self.position[7]]
		elif self.position[1].getKode() == self.position[4].getKode() == self.position[7].getKode() != ".":
			return [True,self.position[1]]
		elif self.position[2].getKode() == self.position[5].getKode() == self.position[8].getKode() != ".":
			return [True,self.position[2]]
		elif self.position[3].getKode() == self.position[6].getKode() == self.position[9].getKode() != ".":
			return [True,self.position[3]]
		elif self.position[1].getKode() == self.position[5].getKode() == self.position[9].getKode() != ".":
			return [True,self.position[1]]
		elif self.position[3].getKode() == self.position[5].getKode() == self.position[7].getKode() != ".":
			return [True,self.position[3]]
		return [False,'-']
		
	def printBoard(self):
		print("Current board:")
		for k in range(1,10):
			print(self.position[k].getKode(), end=" ")
			if k % 3 == 0:
				print()
		print()