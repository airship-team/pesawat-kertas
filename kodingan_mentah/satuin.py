import random

def hasMove(asal):
	global avail
	global position

	for dest in avail[asal]:
		if canMove(position, asal, dest) and (position[dest] == "."):
			return True
	return False

def canMove(state, a,b):
	global moves
	return (moves[a][b] and state[b] == ".")
	# if move[a][b]:
	#     print("CAN MOVE -->  {} to {}".format(a,b))

def removeObstacle():
	global moves
	moves[tantangan[0]][tantangan[1]] = 1
	moves[tantangan[1]][tantangan[0]] = 1


def addObstacle():
	global moves
	global avail
	global tantangan

	a = random.randint(1,9)
	b = avail[a][random.randint(0,2)]
	while (moves[a][b] == False):
		a = random.randint(1,10)
		b = avail[a][random.randint(0,2)]
	moves[a][b] = 0
	moves[b][a] = 0
	tantangan[0] = a
	tantangan[1] = b
	#move[tantangan[0]][tantangan[1]] = False

def printBoard():
	global position

	print("Current board:")
	for k in range(1,10):
		print(position[k], end=" ")
		if k % 3 == 0:
			print()
	print()


# Asumsi :
#     Pawn yang dipindahkan telah dicek bahwa itu adalah pawn milik pemiliknya
#    canMove() telah tercek bernilai True
def move(position, asal, tujuan):
	# Swap position
	tempo = position[asal]
	position[asal] = position[tujuan]
	position[tujuan] = tempo

def isPawn(idx):
	if position[idx] != ".":
			return True
	return False

def heuristic(state, player):
	state_now = [
	[state[1],state[2],state[3]],
	[state[4],state[5],state[6]],
	[state[7],state[8],state[9]],
	[state[1],state[4],state[7]],
	[state[2],state[5],state[8]],
	[state[3],state[6],state[9]],
	[state[1],state[5],state[9]],
	[state[3],state[5],state[7]]]
	# state_count = []
	maks = 0
	for i in range(len(state_now)):
		if i == 0 and player == "x":
			temp = 0
		elif i == 2 and player == "o":
			temp = 0
		else:
			temp = state_now[i].count(player)
		if player == kodePlayer:
			temp == -temp
		if maks < temp:
			maks = temp
	return maks

def wins(state, player):
	win_state = [
	[state[1],state[2],state[3]],
	[state[4],state[5],state[6]],
	[state[7],state[8],state[9]],
	[state[1],state[4],state[7]],
	[state[2],state[5],state[8]],
	[state[3],state[6],state[9]],
	[state[1],state[5],state[9]],
	[state[3],state[5],state[7]],
	]

	for i in range(len(win_state)):
		if [player, player, player] == win_state[i]:
			if (player == "x" and i == 0):
				return False
			elif player == "o" and i == 2:
				return False
			return player
	return False

def minimax(state, depth, player):
	if player == True: # player
		best = [-1, -1, 0]
	else: # ai nya
		best = [-1, -1, 0]

	# Kalau udah ada yang menang di state sekarang
	if wins(state, kodePlayer):
		return [-1,-1,-3]
	elif wins(state, kodeAI):
		return [-1,-1, 3]

	if depth == 0:
		if player:
			return [-1,-1,heuristic(state, kodePlayer)]
		else:
			return [-1,-1,heuristic(state, kodeAI)]

	for x in range(1,10):
		if (player and (state[x] != kodePlayer)) or ((not player) and (state[x] != kodeAI)):
			continue
		tempState = state[:]
		for y in avail[x]:
			if not canMove(tempState, x, y):
				continue

			move(tempState, x, y)
			score = minimax(tempState, depth-1, not player)
			move(tempState, x, y)
			score[0], score[1] = x, y

			if best[0] == -1:
				best = score
			elif player == 1:
				if score[2] < best[2]:
					best = score
			else:
				if score[2] > best[2]:
					best = score
	return best

def aiMoveMinimax():
	global position

	hasil = minimax(position[:], 5, False)
	asal = hasil[0]
	tujuan = hasil[1]
	print("enemyPawns moves from position {} to position {}".format(asal, tujuan))

	move(position, asal, tujuan)

# Ini yang random
def aiMove():
	global position

	asal = random.randint(1,9)
	while (position[asal] != kodeAI):
		asal = random.randint(1,9)

	while not hasMove(asal):
		#print(pion)
		asal = enemyPawns[random.randint(0,2)]

	tujuan = avail[asal][random.randint(0,2)]
	while (not canMove(position, asal, tujuan) or (isPawn(tujuan))):
		tujuan = avail[asal][random.randint(0,2)]

	print("enemyPawns moves from position {} to position {}".format(asal, tujuan))
	move(position, asal, tujuan)

def playerMove():
	global position

	asal = int(input("Masukkan posisi pawn yang ingin anda pindahkan: "))
	while (position[asal] != kodePlayer) or (not hasMove(asal)):
		if not hasMove(asal):
			print("Pion pada posisi {} tidak bisa dipindahkan!".format(asal))
		else:
			print("Pion pada posisi {} bukan pion milik anda.".format(asal))
			asal = int(input("Masukkan kembali posisi pawn yang ingin dipindahkan : "))
			print()

	tujuan = int(input("Masukkan posisi tujuan :"))

	while (isPawn(tujuan) or not (canMove(position, asal, tujuan))):
		print("Pion anda di posisi {} tidak bisa berjalan menuju posisi {}".format(asal,tujuan))
		tujuan = int(input('Masukkan kembali posisi tujuan :'))
		print()

	move(position, asal, tujuan)
	###############
	### SETTING KALAU SALAH YANG TUJUAN BISA GANTI AWALANNYA JUGA
	###########

def gameStart():
	global playerTurn
	winner = False

	while not winner:
		printBoard()
		if dif == 3 : #add tantangan 
			addObstacle()
		if playerTurn:
			playerMove()
			playerTurn = not playerTurn
			winner = wins(position, kodePlayer)
		else:
			if dif == 1:
				aiMove()   	 # GANTI KALAU RANDOM DAN TIDAK
			elif dif == 2:
				aiMoveMinimax()
			playerTurn = not playerTurn
			winner = wins(position, kodeAI)
		removeObstacle() #remove tantangan
		print(winner)
	printBoard()
	print("{} win!".format(winner))

######################################
#### MAIN MAIN MAIN MAIN MAIN ##########
#######################################

# Inisisasi variabel global dengan dummy
playerTurn = 0

# Inisiasi board
position = [[]]
for i in range(1,4):
	position.append("x")
for i in range(1,4):
	position.append(".")
for i in range(1,4):
	position.append("o")



# Inisiasi available move
moves = [[False]]
avail = [[],[2,4,5],[1,3,5],[2,6,5],[1,7,5],[1,2,3,4,6,7,8,9],[3,9,5],[4,8,5],[7,9,5],[6,8,5]]
for i in range(1,10):
	moves.append([False])   			 # Untuk index 0 pasti False
	for j in range(1,10):
		if j in avail[i]:
			moves[i].extend([1])
		else:
			moves[i].extend([0])

print("Pilih tingkat kesulitan:")
print("1: Easy, 2: Medium, 3: Hard")
dif = int(input("Tingkat kesulitan : "))

kodePlayer = input("Pilih pion (x or o): ")
if kodePlayer == 'x':
	playerTurn = False
	kodeAI = 'o'
elif kodePlayer == "o":
	playerTurn = True
	kodeAI = 'x'
else:
	input("Maaf. Input salah")
	exit()

# Inisiasi variabel untuk tantangan untuk kesulitan "Hard"
tantangan = [1,2]

print("Informasi posisi board :")
print("1 2 3")
print("4 5 6")
print("7 8 9")

print('=' * 25 + " Game Start!!! " * 3 + '=' * 25)
print()

gameStart()
# minimax()
