class Pawn:
	def __init__(self, number, position):
		self.kode = number
		self.position = position
		self.pawn = False
		if number == 1:
			self.kode = 'o'
			self.pawn = True
		elif number == 2:
			self.kode = 'x'
			self.pawn = True
		else:
			self.kode = '.'

	def __str__(self):
		# return self.kode
		return "{} {} {}".format(self.kode, self.position, self.pawn)

	def getKode(self):
		return self.kode

	def getPos(self):
		return self.position

	def isPawn(self):
		return self.pawn

	def setPos(self, newPos):
		self.position = newPos