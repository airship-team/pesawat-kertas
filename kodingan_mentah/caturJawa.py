import random
import pawn
import board
from board import Board
from pawn import Pawn

def initializing():
	global board 		# Board
	global position		# Posisi dari board sekarang
	global player 		# Posisi dari si player
	global enemy 		# Posisi dari enemy / AI

	board = Board()
	position = [[]]
	for i in range(1,10):
		position.append(Pawn(0,i))
	for i in range(1,4):
		position[i] = Pawn(2,i)
		if playerTurn:
			enemy.append(position[i])
		else:
			player.append(position[i])
	for i in range(7,10):
		position[i] = Pawn(1,i)
		if playerTurn:
			player.append(position[i])
		else:
			enemy.append(position[i])
	board.update(position)

def canMove(asal, tujuan):
	global board
	return board.getAvailableMove(asal, tujuan)

# Asumsi :
# 	Pawn yang dipindahkan telah dicek bahwa itu adalah pawn milik pemiliknya
#	canMove() telah tercek bernilai True
def move(pawn, tujuan):
	global position
	asal = pawn.getPos()
	pawnTujuan = position[tujuan]

	# Ganti posisi dalam objek pawn-nya
	pawn.setPos(tujuan)
	pawnTujuan.setPos(asal)
	
	# Swap position
	tempo = position[asal]
	position[asal] = position[tujuan]
	position[tujuan] = tempo

	board.update(position)
	############

def isPawn(pawn):
	return pawn.isPawn()

def hasMove(pawn):
	global board
	return board.hasMove(pawn)

def aiMove():
	global enemy
	global position

	pion = enemy[random.randint(0,2)]
	while not hasMove(pion):
		print(pion)
		pion = enemy[random.randint(0,2)]

	asal = pion.getPos()
	tujuan = random.randint(1,9)
	while (not canMove(asal, tujuan) or (asal == tujuan) or (isPawn(position[tujuan]))):
		tujuan = random.randint(1,9)
	
	print("Enemy moves from position {} to position {}".format(pion.getPos(), tujuan))
	move(pion, tujuan)

def playerMove():
	global player
	global position

	curPawn = int(input("Masukkan posisi pawn yang ingin anda pindahkan: "))
	while (position[curPawn] not in player) or (not hasMove(position[curPawn])):
		if not hasMove(position[curPawn]):
			print("Pion pada posisi {} tidak bisa dipindahkan!".format(curPawn))
		else:
			print("Pion pada posisi {} bukan pion milik anda.".format(curPawn))
		curPawn = int(input("Masukkan kembali posisi pawn yang ingin dipindahkan : "))
		print()
	
	tujuan = int(input("Masukkan posisi tujuan :"))
	
	while (isPawn(position[tujuan])) or not (canMove(curPawn, tujuan)):
		print("Pion anda di posisi {} tidak bisa berjalan menuju posisi {}".format(curPawn,tujuan))
		tujuan = int(input('Masukkan kembali posisi tujuan :'))
		print()

	move(position[curPawn], tujuan)

def gameStart():
	global board
	global playerTurn
	condition = [False, '-']
	
	while not condition[0]:
		board.printBoard()
		if playerTurn:
			playerMove()
			playerTurn = not playerTurn
		else:
			aiMove()
			playerTurn = not playerTurn
		condition = board.check()

	board.printBoard()
	print("{} win!".format(condition[1].getKode()))

# Inisisasi variabel global dengan dummy
board = 0
playerTurn = 0

# Inisasi list kosong yang akan dipakai
position = []
player = []
enemy = []

kode = input("Pilih pion (x or o): ")
if kode == 'x':
	playerTurn = False
elif kode == "o":
	playerTurn = True
else:
	input("Maaf. Input salah")
	exit()

print("Informasi posisi board :")
print("1 2 3")
print("4 5 6")
print("7 8 9")

print('=' * 25 + " Game Start!!! " * 3 + '=' * 25)
print()
initializing()
gameStart()
# minimax()
