from django.urls import path
from app_board import views

app_name = 'app_board'

urlpatterns = [
	path('', views.board, name='board'),
]
